#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <deque>

std::vector<unsigned int> generateSeq(){
	// this code taken from Erik Falor's code in the assignment description
	std::random_device rd;
	std::mt19937 engine{rd()};
	std::uniform_int_distribution<unsigned int> dist{1, 250};	// {page minmimum, page maximum}
	
	std::vector<unsigned int> sequence;
	sequence.resize(1000);
	std::generate(sequence.begin(), sequence.end(), [&](){return dist(engine);});
	
	return sequence;
}

void reportAnomaly(unsigned int seqNum, int currFaults, int prevFaults, unsigned int currFrame){
	std::cout << "Anomaly Discovered!" << std::endl;
	std::cout << "	" << "Sequence: " << seqNum + 1 << std::endl;
	std::cout << "	" << "Page Faults: " << prevFaults << " @ ";
	std::cout << "frame Size: " << currFrame << std::endl;
	std::cout << "	" << "Page Faults: " << currFaults << " @ ";
	std::cout << "frame Size: " << currFrame + 1 << std::endl << std::endl;
}

int main(){
	int currFaults = 0;
	int prevFaults = 0;
	int anomalyCount = 0;
	bool foundVal;
	std::deque<unsigned int> frame;
	
	for(unsigned int i = 0; i < 100; i++){	// number of sequences
		std::vector<unsigned int> sequence = generateSeq();
		
		for(unsigned int j = 0; j < 100; j++){	// size of frames
			frame.clear();
			
			for(unsigned int s = 0; s < sequence.size(); s++){	// loop through sequence
				foundVal = false;
				
				if(s <= j){		// sequence position <= frame size
					currFaults++;
					frame.push_back(sequence[s]);
				}else{
					for(unsigned int f = 0; f < frame.size(); f++){	// check frames
						if(sequence[s] == frame[f]){
							foundVal = true;
							break;
						}
					}
					
					if(!foundVal){
						currFaults++;
						frame.pop_front();
						frame.push_back(sequence[s]);
					}
				}
			}
			
			if(prevFaults < currFaults && j != 0){	// anomaly found
				anomalyCount++;
				reportAnomaly(i, currFaults, prevFaults, j);
			}
			
			prevFaults = currFaults;
			currFaults = 0;
		}
	}
	
	std::cout << std::endl << "Anomaly detected " << anomalyCount << " times." << std::endl;
	
	return 0;
}
